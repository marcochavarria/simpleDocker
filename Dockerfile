FROM maven:3-jdk-8
ADD . /DockerJunit
WORKDIR /DockerJunit
 
RUN apt-get update

# Compile app 
RUN mvn clean install && \
    chmod 755 ./start_app.sh && \
	chmod 755 ./tests/test_script.sh && \
    chmod 755 ./JTests/testTool.sh
	
# Run the app 
ENTRYPOINT ["./start_app.sh"]

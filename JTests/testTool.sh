#!/bin/sh

echo "Lets start the evaluation"

searchKey='failures=\"0\"'
errors=0

for f in ./target/surefire-reports/*.xml
do 
  grep -r "$searchKey" $f
  
  if [ $? -gt 0 ]
  then
	echo "ERRORS FOUND for $f"
	errors=1
  else 
	echo "TEST PASSED for $f "	
  fi
done 
 
cat ./target/surefire-reports/*.txt
if [ $errors -gt 0 ]
   then
	exit 1 
  else 
	exit 0
fi


